package com.service;


import com.model.Wine;
import com.repository.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WineService {


    @Autowired
    private WineRepository wineRepository;

    public Wine findById(long id) {
        return wineRepository.findOne(id);
    }

    public void update(Wine person1) {
        wineRepository.save(person1);
    }

    public void deleteById(long id) {
        wineRepository.delete(id);
    }

    public void save(Wine person) {
        wineRepository.save(person);
    }

    public List<Wine> findAll() {

        return wineRepository.findAll();
    }
}
