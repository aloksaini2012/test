package com.controller;


import com.model.Person;
import com.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    public static final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Person>> listAllPerson() {
        List<Person> persons = personService.findAll();
        if (persons.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Person>>(persons, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public ResponseEntity<?> getPerson(@PathVariable("id") long id) {
        logger.info("Fetching Person with id {}", id);
        Person person = personService.findById(id);
        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }

    @RequestMapping(value = "/create/", method = RequestMethod.POST)
    public ResponseEntity<?> createPerson(@RequestBody Person person, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Person : {}", person);
        personService.save(person);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/persion/{id}").buildAndExpand(person.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Person person) {
        logger.info("Updating Person with id {}", id);
        Person person1 = personService.findById(id);
        if (person1 == null) {
            logger.error("Unable to update. Persion with id {} not found.", id);
            return new ResponseEntity("Persion not found", HttpStatus.NOT_FOUND);
        }
        person1.setFirstName(person.getFirstName());
        personService.update(person1);
        return new ResponseEntity<Person>(person1, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        personService.deleteById(id);
        return new ResponseEntity<Person>(HttpStatus.NO_CONTENT);
    }
}
