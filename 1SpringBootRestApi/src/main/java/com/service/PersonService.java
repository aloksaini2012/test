package com.service;



import com.model.Person;
import com.repository.PersonRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PersonService {
    private PersonRepository personRepository;

    public List<Person> findAll() {

        return personRepository.findAll();
    }

    public Person findById(long id) {
        return personRepository.findOne(id);
    }

    public void save(Person person) {
        personRepository.save(person);
    }

    public void update(Person person1) {
        personRepository.save(person1);
    }

    public void deleteById(long id) {
       personRepository.delete(id);
    }
}
