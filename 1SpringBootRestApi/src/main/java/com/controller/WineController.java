package com.controller;

import com.model.Wine;
import com.service.WineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/wine")
public class WineController {


    public static final Logger logger = LoggerFactory.getLogger(WineController.class);

    @Autowired
    private WineService wineService;



    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Wine>> listAllWine() {
        List<Wine> persons = wineService.findAll();
        if (persons.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Wine>>(persons, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public ResponseEntity<?> getwine(@PathVariable("id") long id) {
        logger.info("Fetching wine with id {}", id);
        Wine person = wineService.findById(id);
        return new ResponseEntity<Wine>(person, HttpStatus.OK);
    }

    @RequestMapping(value = "/create/", method = RequestMethod.POST)
    public ResponseEntity<?> createWine(@RequestBody Wine person, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Wine : {}", person);
        wineService.save(person);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/user/{id}").buildAndExpand(person.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Wine wine) {
        logger.info("Updating User with id {}", id);
        Wine wine1 = wineService.findById(id);
        if (wine1 == null) {
            logger.error("Unable to update. Wine with id {} not found.", id);
            return new ResponseEntity("Wine not found", HttpStatus.NOT_FOUND);
        }
        wine1.setWineName(wine.getWineName());
        wine1.setPrice(wine.getPrice());
        wineService.update(wine1);
        return new ResponseEntity<Wine>(wine1, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        wineService.deleteById(id);
        return new ResponseEntity<Wine>(HttpStatus.NO_CONTENT);
    }


}
